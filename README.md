# Tined

A GTK+ scratchpad for use with a tiling WM

## Usage

| Action | Hotkey |
| ------ | ------ |
| <kbd>Ctrl</kbd>+<kbd>O</kbd> | Open |
| <kbd>Ctrl</kbd>+<kbd>S</kbd> | Save |
| <kbd>Ctrl</kbd>+<kbd>F</kbd> | ~~Quick search~~ |
