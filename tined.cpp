#include <iostream>
#include <fstream>
#include <cstdint>
#include <gtkmm-2.4/gtkmm.h>

#define DEFAULT_FILENAME "Unnamed"

class TinedWindow : public Gtk::Window {
public:
    TinedWindow();
    virtual ~TinedWindow();
    bool on_key_press(GdkEventKey* eventKey);
    bool on_key_release(GdkEventKey* eventKey);
    bool on_delete(GdkEventAny* event);
    void on_resize();
    void on_search();

private:
    void file_open();
    bool file_save();
    bool handle_modified(const char * secondary_text);
    void search_popup_show();
    void search_popup_hide();

    Gtk::Entry m_find_entry;

    Gtk::Layout m_layout;
    Gtk::ScrolledWindow m_scrolled_window;
    Gtk::TextView m_text_view;
    Glib::RefPtr<Gtk::TextBuffer> m_text_buffer;
    bool m_ctrl_modifier;
    Glib::ustring m_filename;
};

TinedWindow::TinedWindow() : m_ctrl_modifier(false), m_filename(DEFAULT_FILENAME)
{
    set_title("tined");
    set_border_width(2);
    set_default_size(360, 210);

    // set font, custom tab behaviour
    Pango::FontDescription font;
    font.set_family("Noto Sans Mono");
    font.set_size(11 * PANGO_SCALE);
    m_text_view.modify_font(font);
    m_text_view.set_accepts_tab(false);

    // handle key bindings via key press events,
    // this is needed due to limitations on gtk2 accelerators
    signal_key_press_event().connect(sigc::mem_fun(*this, &TinedWindow::on_key_press));
    signal_key_release_event().connect(sigc::mem_fun(*this, &TinedWindow::on_key_release));

    signal_delete_event().connect(sigc::mem_fun(*this, &TinedWindow::on_delete));
    signal_check_resize().connect(sigc::mem_fun(*this, &TinedWindow::on_resize));
    m_text_buffer = m_text_view.get_buffer();

    // connect Enter key to on_search
    m_find_entry.signal_activate().connect(sigc::mem_fun(*this, &TinedWindow::on_search));

    m_scrolled_window.set_policy(Gtk::PolicyType::POLICY_AUTOMATIC, Gtk::PolicyType::POLICY_AUTOMATIC);
    m_scrolled_window.add(m_text_view);

    m_layout.add(m_scrolled_window);
    m_layout.add(m_find_entry);
    add(m_layout);

    m_layout.show();
    m_scrolled_window.show();
    m_text_view.show();
}

TinedWindow::~TinedWindow() {}

bool TinedWindow::on_key_press(GdkEventKey* eventKey)
{
    if (eventKey->keyval == GDK_KEY_Control_L || eventKey->keyval == GDK_KEY_Control_R)
        m_ctrl_modifier = true;

    // bind Ctrl + O to file_open()
    if (eventKey->keyval == GDK_KEY_o && m_ctrl_modifier)
        this->file_open();

    // bind Ctrl + S to file_save()
    if (eventKey->keyval == GDK_KEY_s && m_ctrl_modifier)
        this->file_save();

    // show search on Ctrl + F
    if (eventKey->keyval == GDK_KEY_f && m_ctrl_modifier)
        search_popup_show();

    // hide search on Esc
    if (eventKey->keyval == GDK_KEY_Escape)
        search_popup_hide();

    return true;
}

bool TinedWindow::on_key_release(GdkEventKey* eventKey)
{
    if (eventKey->keyval == GDK_KEY_Control_L || eventKey->keyval == GDK_KEY_Control_R)
        m_ctrl_modifier = false;

    // insert 4 spaces instead of dealing with tabs
    if (eventKey->keyval == GDK_KEY_Tab && m_text_view.has_focus())
        m_text_buffer->insert_at_cursor("    ");

    return true;
}


bool TinedWindow::on_delete(GdkEventAny* event)
{
    // make sure the buffer is saved before exiting
    const char * secondary_text =
        "If you close the file without saving, your changes will be discarted.";
    return ! handle_modified(secondary_text);
}

// as Gtk::Layout doesn't handle resizes adjust it's children accordingly
// (A better solution would be Gtk::Overlay, which is available from Gtk3 onwards)
void TinedWindow::on_resize()
{
    static int old_width, old_height;
    int width, height;
    get_size(width, height);

    if (old_width == width && old_height == height ||
        width <= 4 || height <= 4)
        return;

    old_width = width;
    old_height = height;

    m_scrolled_window.set_size_request(width - 4, height - 4);
    m_layout.move(m_find_entry, width - 174, 4);
}

// handles a dirty text buffer
// returns false on RESPONSE_CANCEL, or if the file_save didn't succeed
bool TinedWindow::handle_modified(const char * secondary_text)
{
    if (m_text_buffer->get_modified())
    {
        Glib::ustring heading;
        heading += "Save changes to '";
        heading += m_filename;
        heading += "'?";

        Gtk::MessageDialog dialog(
            *this,
            heading,
            false,
            Gtk::MESSAGE_QUESTION,
            Gtk::BUTTONS_NONE
        );

        dialog.set_secondary_text(secondary_text);
        dialog.add_button(Gtk::Stock::NO, Gtk::RESPONSE_NO);
        dialog.add_button(Gtk::Stock::CANCEL, Gtk::RESPONSE_CANCEL)->grab_focus();
        dialog.add_button(Gtk::Stock::YES, Gtk::RESPONSE_YES);

        switch (dialog.run())
        {
            case Gtk::RESPONSE_NO:
                break;
            case Gtk::RESPONSE_CANCEL:
                return false;
            case Gtk::RESPONSE_YES:
                return this->file_save();
        }
    }

    return true;
}

void TinedWindow::file_open()
{
    Glib::ustring new_filename;

    // gracefully handle a modified buffer
    const char * secondary_text =
        "If you open a different file without saving, your changes will be discarted.";
    if (! handle_modified(secondary_text))
        return;

    // spawn a file chooser
    {
        Gtk::FileFilter filter_text, filter_any;
        filter_text.set_name("Text files");
        filter_text.add_mime_type("text/plain");
        filter_any.set_name("Any files");
        filter_any.add_pattern("*");

        Gtk::FileChooserDialog dialog(
            "Please choose a text file",
            Gtk::FILE_CHOOSER_ACTION_OPEN
        );

        dialog.set_transient_for(*this);
        dialog.add_button(Gtk::Stock::CANCEL, Gtk::RESPONSE_CANCEL);
        dialog.add_button(Gtk::Stock::OPEN, Gtk::RESPONSE_OK);
        dialog.add_filter(filter_text);
        dialog.add_filter(filter_any);

        if (dialog.run() != Gtk::RESPONSE_OK)
            return;

        new_filename = dialog.get_filename();
    }

    // try to open the file
    std::ifstream fs;
    fs.open(new_filename, std::ifstream::in);

    if (! fs.good())
    {
        Glib::ustring heading;
        heading += "Failed to open '";
        heading += new_filename;
        heading += "'.";

        Gtk::MessageDialog dialog(
            *this,
            heading,
            false,
            Gtk::MESSAGE_ERROR,
            Gtk::BUTTONS_OK
        );

        dialog.run();

        return;
    }

    // dump file contents into m_text_buffer
    std::stringstream ss;
    ss << fs.rdbuf();
    fs.close();
    m_text_buffer->set_text(ss.str());

    m_filename = new_filename;
}

bool TinedWindow::file_save()
{
    Glib::ustring new_filename = m_filename;

    // don't save unless it's actually necessary
    if (!m_text_buffer->get_modified() && m_filename != DEFAULT_FILENAME) return true;

    // ask for a path/filename if necessary
    if (m_filename == DEFAULT_FILENAME)
    {
        Gtk::FileFilter filter_text, filter_any;
        filter_text.set_name("Text files");
        filter_text.add_mime_type("text/plain");
        filter_any.set_name("Any files");
        filter_any.add_pattern("*");

        Gtk::FileChooserDialog dialog(
            "Please choose a text file",
            Gtk::FILE_CHOOSER_ACTION_SAVE
        );

        dialog.set_transient_for(*this);
        dialog.add_button(Gtk::Stock::CANCEL, Gtk::RESPONSE_CANCEL);
        dialog.add_button(Gtk::Stock::SAVE, Gtk::RESPONSE_OK);
        dialog.add_filter(filter_text);
        dialog.add_filter(filter_any);
        dialog.set_current_name(m_filename);

        if (dialog.run() != Gtk::RESPONSE_OK)
            return false;

        new_filename = dialog.get_filename();
    }

    // try to open/create the file
    std::ofstream fs;
    fs.open(new_filename, std::ofstream::out);

    if (! fs.good())
    {
        Glib::ustring heading;
        heading += "Failed to open '";
        heading += new_filename;
        heading += "'.";

        Gtk::MessageDialog dialog(
            *this,
            heading,
            false,
            Gtk::MESSAGE_ERROR,
            Gtk::BUTTONS_OK
        );

        dialog.run();

        return false;
    }

    // save file
    std::cout << "Saving to: " << new_filename << std::endl;
    fs << m_text_buffer->get_text();
    fs.close();
    m_text_buffer->set_modified(false);
    m_filename = new_filename;

    return true;
}

void TinedWindow::search_popup_show()
{
    m_find_entry.show();
    m_find_entry.grab_focus();
}

void TinedWindow::search_popup_hide()
{
    m_find_entry.hide();
    m_text_view.grab_focus();
}

void TinedWindow::on_search()
{
    Glib::ustring token = m_find_entry.get_text();
    Glib::ustring text = m_text_buffer->get_text();

    for (size_t token_pos = 0; ; token_pos += token.size())
    {
        // find all occurences
        token_pos = text.find(token, token_pos);
        if (token_pos == SIZE_MAX)
            break;

        std::cout << "token_pos: " << token_pos << std::endl;

        // highlight all occurences
        // (remove all tags, add new tags)


        // set selection, add tags

        // cycle

        // escape to select
    }
}

int main(int argc, char *argv[])
{
    Gtk::Main kit(argc, argv);

    TinedWindow tinedWindow;

    Gtk::Main::run(tinedWindow);

    return 0;
};

